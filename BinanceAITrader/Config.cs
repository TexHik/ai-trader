﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;

namespace BinanceAITrader
{
    public class Config
    {
        public static Config globalcfg = new Config("./config");

        #region secret
        public string binance_token = "PWNA98dh9NLtuhyOqydR9qeCUrlIySruEFcW54AnuXVW1FxxcyS2DXQvRavROANQ";
        public string binance_secret = "e2UnqhnEYJiW4eubv1rpEToTn94o6S3iydC6HPD6yLeRIi2IQ8nk1ou7oinZMYIq";
        #endregion secret

        #region RPC
        public string grpc_host = "http://localhost:50052";
        #endregion RPC

        #region Trading
        #endregion Trading

        public Config() { }
        public Config(string filename)
        {
            if (!File.Exists(filename))
            {
                File.WriteAllText(filename, JsonConvert.SerializeObject(this, Formatting.Indented));
            }
            else
            {
                Config cfg = JsonConvert.DeserializeObject<Config>(File.ReadAllText(filename));
                var thisfields = GetType().GetFields();
                foreach (var field in cfg.GetType().GetFields())
                {
                    thisfields.First(p => p.Name == field.Name).SetValue(this, field.GetValue(cfg));
                }
            }

        }
    }
}
