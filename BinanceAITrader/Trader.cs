﻿using Binance.Net;
using Binance.Net.Objects.Spot.SpotData;
using CryptoExchange.Net.Authentication;
using CryptoExchange.Net.Logging;
using CryptoExchange.Net.Objects;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace BinanceAITrader
{
    public class Trader
    {
        public BinanceClient client;
        public BinanceSocketClient socketClient;
        string baseName, quoteName, symbol;
        int windowsize;



        Task trader;



        public double actualAskPrice;
        public double actualAskQuantity;
        public double actualBidPrice;
        public double actualBidQuantity;

        public double actualBTCBalance;

        public double actualSymbolBalance;


        double last_prediction = 0;
        public Trader(string key, string secret, string baseName, string quoteName, int windowsize)
        {
            client = new BinanceClient(new Binance.Net.Objects.Spot.BinanceClientOptions
            {
                ApiCredentials = new ApiCredentials(key, secret),
                LogVerbosity = LogVerbosity.Debug,
                TimestampOffset = TimeSpan.FromSeconds(1)
            });
            this.baseName = baseName;
            this.quoteName = quoteName;
            symbol = client.GetSymbolName(baseName, quoteName);
            this.windowsize = windowsize;
            var startResult = client.Spot.UserStream.StartUserStream();
            if (!startResult.Success)
                throw new Exception($"Failed to start user stream: {startResult.Error}");
            socketClient = new BinanceSocketClient(new Binance.Net.Objects.Spot.BinanceSocketClientOptions { AutoReconnect = true, ReconnectInterval = TimeSpan.FromMilliseconds(100) });
            socketClient.Spot.SubscribeToBookTickerUpdates(symbol, (data) =>
            {
                actualAskPrice = (double)data.BestAskPrice;
                actualAskQuantity = (double)data.BestAskQuantity;

                actualBidPrice = (double)data.BestBidPrice;
                actualBidQuantity = (double)data.BestBidQuantity;
            });
            socketClient.Spot.SubscribeToUserDataUpdates(startResult.Data,
                accountUpdate => 
                {
                },
                orderUpdate =>
                {
                    Console.WriteLine($"Order updated: {orderUpdate.OrderId} - {orderUpdate.QuantityFilled}/{orderUpdate.Quantity}");
                },
                ocoUpdate =>
                {
                    Console.WriteLine($"OCO update: {ocoUpdate.Event}");
                },
                positionUpdate =>
                {
                },
                balanceUpdate =>
                {

                });
            trader = Task.Factory.StartNew(TradingLoop);
        }
      
        public async Task<WebCallResult<BinancePlacedOrder>> OpenOrder(double prediction)
        {
            var marketprice = client.Spot.Market.GetPriceAsync(symbol);
            var accountInfo = client.General.GetAccountInfoAsync();
            var side = prediction > 0 ? Binance.Net.Enums.OrderSide.Buy : Binance.Net.Enums.OrderSide.Sell;

            await marketprice;
            var price = prediction > 0? (double)marketprice.Result.Data.Price*1.02 : (double)marketprice.Result.Data.Price * 0.98;

            var quantity = prediction > 0 ? (double)accountInfo.Result.Data.Balances.First(p => p.Asset == this.quoteName).Free / (double)price :
                (double)accountInfo.Result.Data.Balances.First(p => p.Asset == this.baseName).Free*0.99;

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"Creating {side} order with quantity:{quantity}");
            Console.ResetColor();
            var result = client.Spot.Order.PlaceOrder(symbol, side, Binance.Net.Enums.OrderType.Market,
                quantity: Math.Round((decimal)quantity, 4)
                );
            return result;
        }

        public async Task<WebCallResult<BinancePlacedOrder>> CloseOrder(double prediction)
        {
            var accountInfo = client.General.GetAccountInfoAsync();

            var side = prediction < 0 ? Binance.Net.Enums.OrderSide.Buy : Binance.Net.Enums.OrderSide.Sell;

            var quantity = prediction < 0 ? (double)accountInfo.Result.Data.Balances.First(p => p.Asset == this.quoteName).Free :
                (double)accountInfo.Result.Data.Balances.First(p => p.Asset == this.baseName).Free;

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"Creating {side} order with quantity:{quantity}");
            Console.ResetColor();
            var result = client.Spot.Order.PlaceOrder(symbol, side, Binance.Net.Enums.OrderType.Market,
                quantity: Math.Round((decimal)quantity, 4)
                );
            return result;
        }

        public List<Binance.Net.Interfaces.IBinanceKline> GetKlines(int amount)
        {
            return client.Spot.Market.GetKlines(symbol,Binance.Net.Enums.KlineInterval.FiveMinutes, limit:amount).Data.ToList();//Возвращает свечи, включая текущую.
        }
        private async Task TradingLoop()
        {
            while (true)
            {
                try
                {
                    List<Binance.Net.Interfaces.IBinanceKline> klines = GetKlines(windowsize+2);
                    if (DateTime.Now.ToUniversalTime().Subtract(klines.Last().OpenTime).TotalSeconds>10)
                    {
                        Console.ResetColor();
                        Console.WriteLine($"Waiting next trading window in {(int)klines.Last().CloseTime.Subtract(DateTime.Now.ToUniversalTime()).TotalSeconds}s");
                        Thread.Sleep((int)klines.Last().CloseTime.Subtract(DateTime.Now.ToUniversalTime()).TotalMilliseconds+500);
                        continue;
                    }
                    klines = klines.GetRange(0, windowsize+1);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("[{0:T}]Valid trading window, klines range: ({1:T}-{2:T})", DateTime.Now, klines.First().CloseTime, klines.Last().CloseTime);
                    double dreal = (double)(klines[klines.Count - 1].Close - klines[klines.Count-2].Close);
                    var prediction = RemotePredictior.predictor.predict(klines).CLOSE;

                    if (Math.Sign(prediction) != Math.Sign(last_prediction))
                    {
                        await client.Spot.Order.CancelAllOpenOrdersAsync(symbol);
                        if (prediction != 0)
                        {
                            var order = await OpenOrder(prediction);

                            if (order.Error == null)
                            {
                                Console.WriteLine($"[OPEN] Placed order: {order.Data.Side} {order.Data.Quantity}");
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine($"[OPEN] Order { (prediction > 0 ? "BUY" : "SELL")} placing error: {order.Error}, prediciton: {prediction}");
                            }
                        }
                    }
                    last_prediction = prediction;
                    Console.ResetColor();
                    Thread.Sleep(10000);
                } catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(e);
                    Console.ResetColor();
                }
            }
        }
    }
}
