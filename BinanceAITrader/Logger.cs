﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BinanceAITrader
{
    public class Logger
    {
        public static Logger logger = new Logger("./logs");

        private string dirname;
        public Logger() { }
        public Logger(string dirname)
        {
            if (!Directory.Exists(dirname))
                Directory.CreateDirectory(dirname);
            this.dirname = dirname;
        }



        private string LogFormatter(DateTime timestamp, params object[] args)
        {
            var c = args.ToList().ConvertAll(a => a.ToString()).Aggregate((a, b) => a + " " + b);
            return String.Format("[{0:G}]:  {2}", timestamp, c);
        }
        public void OrderUpdate(long id, double price, string quantity, Binance.Net.Enums.OrderSide side, Binance.Net.Enums.OrderStatus status)
        {
            var path = Path.Combine(this.dirname, "order.log");
            var text = this.LogFormatter(DateTime.Now, id, status, side, price, quantity);
            Console.WriteLine(text);
            File.AppendAllLines(path, new string[] { text });
        }
        public void BalanceChanges(string Symbol, double Variation)
        {
            var path = Path.Combine(this.dirname, "balance.log");
            File.AppendAllLines(path, new string[] { LogFormatter(DateTime.Now, Symbol, Variation > 0 ? $"+{Variation}" : Variation.ToString()) });
        }
    }
}
