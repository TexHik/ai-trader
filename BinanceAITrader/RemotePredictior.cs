﻿using System;
using System.Collections.Generic;
using System.Text;
using GrpcPredictor;
using Grpc.Net.Client;
namespace BinanceAITrader
{
    class RemotePredictior
    {
        public static RemotePredictior predictor = new RemotePredictior(Config.globalcfg.grpc_host);
        Predictor.PredictorClient client;
        public RemotePredictior(string address)
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            var channel = GrpcChannel.ForAddress(address);
            this.client = new Predictor.PredictorClient(channel);
        }
        public GRPCResponce predict(List<Binance.Net.Interfaces.IBinanceKline> klines)
        {
            var req = new GRPCRequest();
            for (int i = 0; i < klines.Count; i++)
            {
                req.CLOSE.Add((double)klines[i].Close);
            }
            return client.predict(req);
        }
    }
}
