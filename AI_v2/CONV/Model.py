import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.regularizers import Regularizer
from tensorflow.keras.utils import plot_model
import numpy as np
import os
#TODO Decrease lstm dimentionaly
os.environ["PATH"] += os.pathsep + 'C:/Users/TexHik/AppData/Local/Programs/Python/Python36/graph/bin'

class InstanceNorm(tf.keras.layers.Layer):
    def __init__(self):
        super(InstanceNorm, self).__init__()
    def build(self, input_shape):
        super(InstanceNorm, self).build(input_shape)
    def call(self, input):
            input -= tf.reduce_mean(input, axis=[1, 2], keepdims=True)
            input *= tf.math.rsqrt(tf.reduce_mean(tf.square(input), axis=[1,2], keepdims=True) + 1e-8)
            return input

class BinaryPredictor():
    def __init__(self,
                 window_size,
                 depth = 6,
                 lr=1e-3,
                 training = False
                 ):
        self.window_size = window_size
        self.depth = depth
        self.lr = lr

        self.predictor = self._predictor()
        self.optimizer = tf.keras.optimizers.Adam(self.lr)
        self.trainstep = 0
        self.epoch = 0
        if (training):
            self.writer = tf.summary.create_file_writer("./tensorboard/")

        print("Predictor created depth {} \n".format(self.depth))
        print("Total amount of predictor trainable parameters: {}\n"
            .format(
            np.sum([np.prod(v.get_shape().as_list()) for v in self.predictor.trainable_variables]),
        ))

        plot_model(self.predictor, to_file='./predictor.png', show_shapes=True, show_layer_names=True)

    def _predictor(self):
        i1 = layers.Input(shape=[self.window_size,3])
        l1 = tf.expand_dims(i1,axis=3)

        with tf.device("/gpu:0"):
            l1 = layers.Conv2D(4, kernel_initializer='he_normal', kernel_size=(5,1), use_bias=True, padding='valid')(l1)
            l1 = InstanceNorm()(l1)
            l1 = layers.Conv2D(8, kernel_initializer='he_normal', kernel_size=(3,1), use_bias=True, padding='valid')(l1)
            l1 = InstanceNorm()(l1)

            l1 = layers.MaxPool2D(pool_size=(2,1))(l1)

            l1 = layers.Conv2D(16, kernel_initializer='he_normal', kernel_size=(3,1), use_bias=True, padding='same')(l1)
            l1 = InstanceNorm()(l1)
            l1 = layers.Conv2D(32, kernel_initializer='he_normal', kernel_size=(3,1), use_bias=True, padding='same')(l1)
            l1 = InstanceNorm()(l1)
            l1 = layers.MaxPool2D(pool_size=(2,1))(l1)

            r1 = l1[:, :, 0, :]
            r2 = l1[:, :, 1, :]
            r3 = l1[:, :, 2, :]
        with tf.device("/cpu:0"):
            for i in range(3):
                r1 = layers.LSTM(32, return_sequences=True, kernel_initializer='he_normal', dropout=0.1, recurrent_dropout=0.1)(r1)
                r2 = layers.LSTM(32, return_sequences=True, kernel_initializer='he_normal', dropout=0.1, recurrent_dropout=0.1)(r2)
                r3 = layers.LSTM(32, return_sequences=True, kernel_initializer='he_normal', dropout=0.1, recurrent_dropout=0.1)(r3)
            r1 = layers.LSTM(32, kernel_initializer='he_normal')(r1)
            r2 = layers.LSTM(32, kernel_initializer='he_normal')(r2)
            r3 = layers.LSTM(32, kernel_initializer='he_normal')(r3)
        with tf.device("/gpu:0"):
            r1 = layers.Dense(32)(r1)
            r2 = layers.Dense(32)(r2)
            r3 = layers.Dense(32)(r3)

            r1 = layers.Dense(1)(r1)
            r2 = layers.Dense(1)(r2)
            r3 = layers.Dense(1)(r3)
            o = layers.Concatenate(axis=1)([r1, r2, r3])
            return tf.keras.Model(inputs = i1, outputs = o)

    def loss(self, result, target):
        l1 = tf.reduce_mean(tf.square((target[:, 0] - result[:, 0])), axis=[0], keepdims=False)
        l2 = tf.reduce_mean(tf.square((target[:, 1] - result[:, 1])), axis=[0], keepdims=False)
        l3 = tf.reduce_mean(tf.square((target[:, 2] - result[:, 2])), axis=[0], keepdims=False)
        l = tf.sqrt(l1+l2+l3)
        return l,l1,l2,l3

    def train_step(self, input, target, lr=1e-3):
        if (lr!=self.lr):
            self.lr = lr
            self.optimizer = tf.keras.optimizers.Adam(self.lr)
        with tf.GradientTape() as grad_tape:
            result = tf.cast(self.predictor(input), dtype='float64')
            target = tf.cast(target, dtype='float64')
            l,l1,l2,l3 = self.loss(result, target)
            with (self.writer.as_default()):
                tf.summary.scalar("Loss", l,
                                  step=self.trainstep)
                tf.summary.scalar("Loss l1", l1,
                                  step=self.trainstep)
                tf.summary.scalar("Loss l2", l2,
                                  step=self.trainstep)
                tf.summary.scalar("Loss l3", l3,
                                  step=self.trainstep)
                tf.summary.histogram("result", result, step=self.trainstep)
                self.writer.flush()
            grads = grad_tape.gradient(l, self.predictor.trainable_variables)
            self.optimizer.apply_gradients(zip(grads, self.predictor.trainable_variables))
            self.trainstep+=1
            return l, result
    def save(self):
        np.save("./model/info.npy",[self.trainstep,self.epoch])
        self.predictor.save_weights("./model/")
    def load(self):
        data = np.load("./model/info.npy")
        self.trainstep = data[0]
        self.epoch = data[1]
        self.predictor.load_weights("./model/")