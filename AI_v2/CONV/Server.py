import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.regularizers import Regularizer
from tensorflow.keras.utils import plot_model
import matplotlib.pyplot as plt
from CONV.Model import *
import os, time
import grpc, protolcol_pb2,protolcol_pb2_grpc
from concurrent import futures
import tensorflow as tf
#python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. ./protolcol.proto
from Dataset import Dataset

WINDOW_SIZE = 50

predictor = BinaryPredictor(window_size=WINDOW_SIZE)
predictor.load()
print("Warming up")
predictor.predictor(np.zeros(shape=[1,WINDOW_SIZE,3]))
print("Warmup complete")


history_real_close = []
history_prediction_close = []

class Predictor(protolcol_pb2_grpc.PredictorServicer):
    def predict(self,request,context):
            drclose = np.array([request.CLOSE[i] for i in range(len(request.CLOSE))],dtype='float64')
            if (len(history_real_close)==0):
                for i in range(len(drclose)):
                    history_real_close.append(drclose[i])
            else:
                history_real_close.append(drclose[-1])

            close,min,max = Dataset.prepare(drclose)
            close = np.expand_dims(close,axis=0)
            prediction = np.array(predictor.predictor(close))

            prediction = (prediction + 1) / 2 * (max - min) + min

            prediction = np.sum(prediction,axis=1)
            history_prediction_close.append(prediction[0])
            response = protolcol_pb2.GRPCResponce(CLOSE=prediction[0])
            return response
    def history(self, request, context):
        response = protolcol_pb2.History()
        for i in range(len(history_real_close)):
            response.real_close.append(history_real_close[i])
        for i in range(len(history_prediction_close)):
            response.predicted_close.append(history_prediction_close[i])
        return response

server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
protolcol_pb2_grpc.add_PredictorServicer_to_server(Predictor(), server)
server.add_insecure_port('[::]:50052')
server.start()
print("Server up and running")
try:
    while True:
        time.sleep(860400)
except KeyboardInterrupt:
    server.stop(0)
