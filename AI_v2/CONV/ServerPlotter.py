import numpy as np
import matplotlib.pyplot as plt
import grpc, protolcol_pb2,protolcol_pb2_grpc
import time
def sgn(a):
    if (a>0):
        return 1
    elif(a<0):
        return -1
    else:
        return 0
plt.ion()
last_length = 0

WINDOW_SIZE = 50

channel = grpc.insecure_channel("localhost:50052")
client = protolcol_pb2_grpc.PredictorStub(channel)

figure,ax = plt.subplots(1)

predicted_close, = ax.plot([], [], color='green')
real_close, = ax.plot([], [], color='black')

ax.set_autoscaley_on(True)
ax.set_title('Low')

while(True):
    resp = client.history(protolcol_pb2.Empty())

    real = list(resp.real_close)
    pred = list(resp.predicted_close)


    pred = np.array(pred)
    real = np.array(real)

    if (len(pred)!=last_length):
        ax.set_xlim(0, len(real) + len(pred) + WINDOW_SIZE)
        ax.set_ylim(np.min(real)*0.95, np.max(real)*1.05)


    real_close.set_xdata(list(range(len(real))))
    real_close.set_ydata(real)

    predicted_close.set_xdata(list(range(WINDOW_SIZE,WINDOW_SIZE+len(pred))))
    predicted_close.set_ydata(pred)

    figure.canvas.draw()
    figure.canvas.flush_events()
    last_length = len(pred)

    pr = np.array(pred[:-1])
    rl = np.array(real[-pr.shape[0]:])
    if (len(pr)==len(rl)):
        p = 0
        n = 0
        for i in range(pr.shape[0]-1):
            if (sgn(pr[i+1]-pr[i]) == sgn(rl[i+1]-pr[i])):

                p += 1
            else:
                n+=1
        print("Trend Accuracy Total: {}/{} {}%".format(p, p+n, p * 100.0 / (p+n+1e-8)))

