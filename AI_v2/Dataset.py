import numpy as np
import pandas as pd
import os
import statsmodels.tsa.filters.hp_filter as hp
import matplotlib.pyplot as plt
from sklearn.utils import shuffle

class Dataset():
    def __init__(self, windowsize, lookup = 4, dataname = "../dataset_1m.npy", test_split = 0.9):
        self.test_split = test_split
        self.windowsize = windowsize
        self.lookup = lookup
        self.dataname = dataname
        data = np.load(dataname)

        self.close = np.array(data[0],dtype='float64')
        self.vol = np.array(data[1],dtype='float64')

        #self.dclose = np.array([self.close[i] for i in range(len(self.close)-1)])

        htr, hcyc = hp.hpfilter(self.close,100)
        htr1, hcyc1 = hp.hpfilter(htr, 100)
        self.hclose = np.array([hcyc, hcyc1, htr1])
        self.hclose = self.hclose.swapaxes(0,1)



    def getLength(self):
        return (int((len(self.hclose) - self.windowsize - self.lookup) * self.test_split))


    @staticmethod
    def denorm(val, min, max):
        min = min[:, 0, :]
        max = max[:, 0, :]
        return (val + 1) / 2 * (max - min) + min
    @staticmethod
    def prepare(val):
        htr,hcyc = hp.hpfilter(val,100)
        htr1, hcyc1 = hp.hpfilter(htr, 100)
        hclose = np.array([hcyc, hcyc1, htr1]).swapaxes(0,1)

        min = hclose.min(axis=0, keepdims=True)
        max = hclose.max(axis=0, keepdims=True)
        x = (hclose - min) / (max - min + 1e-8) * 2 - 1
        return x,min,max


    def getTrainData(self, ptr, count, type="delta"):
        assert ptr + count < self.getLength()
        data_x = np.array([self.hclose[i:i+self.windowsize] for i in range(ptr,ptr+count)])
        data_y = np.array([self.hclose[i+self.windowsize] for i in range(ptr,ptr+count)])
        (data_x,data_y) = shuffle(data_x,data_y)
        min = data_x.min(axis=1, keepdims=True)
        max = data_x.max(axis=1, keepdims=True)
        data_x = (data_x - min) / (max - min + 1e-8) * 2 - 1
        data_y = (data_y - min.reshape(-1,data_y.shape[1])) / (max.reshape(-1,data_y.shape[1]) - min.reshape(-1,data_y.shape[1]) + 1e-8) * 2 - 1
        return data_x,data_y,min,max

    def getTestData(self, type="delta"):
        data_x = np.array([self.hclose[i:i + self.windowsize] for i in range(self.getLength(), (len(self.hclose)-self.windowsize-self.lookup))])
        data_y = np.array([self.hclose[i+self.windowsize] for i in range(self.getLength(), (len(self.hclose)-self.windowsize-self.lookup))])
        (data_x, data_y) = shuffle(data_x, data_y)
        min = data_x.min(axis=1, keepdims=True)
        max = data_x.max(axis=1, keepdims=True)
        data_x = (data_x - min) / (max - min + 1e-8) * 2 - 1
        data_y = (data_y - min.reshape(-1,data_y.shape[1])) / (max.reshape(-1,data_y.shape[1]) - min.reshape(-1,data_y.shape[1]) + 1e-8) * 2 - 1
        return data_x,data_y,min,max
    def getSequentialData(self, lth=100):
        return np.array([self.hclose[self.getLength():self.getLength()+lth]])[0]

'''
ds = Dataset(128)
x,y,min,max = ds.getTrainData(0,1000)

x = x[500]
y = y[500]
min = min[500]
max = max[500]
plt.plot(Dataset.denorm(x,min,max)[:,0],color='green')
plt.plot(Dataset.denorm(x,min,max)[:,1],color='yellow')
plt.plot(Dataset.denorm(x,min,max)[:,2],color='blue')

val = Dataset.denorm(x,min,max)
plt.plot(np.sum(val, axis=1), color='red')
plt.plot(ds.close[500:500+128], color='black')
plt.show()
'''