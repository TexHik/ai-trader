import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mplfinance
import statsmodels.tsa.filters.hp_filter
from Dataset import *
def sgn(a):
    if (a>0):
        return 1
    elif(a<0):
        return -1
    else:
        return 0
data = np.load("dataset_1m.npy")
close = np.array(data[0], dtype='float64')
volume = np.array(data[1][1:], dtype='float64')

dclose = np.array([close[i]-close[i-1] for i in range(1,close.shape[0])])

close = [0]
for i in range(dclose.shape[0]):
    close.append(dclose[i])

close = close[:dclose.shape[0]]

p=0
for i in range(len(close)):
    if (sgn(close[i]) == sgn(dclose[i])):
        p += 1
print("Trend Accuracy Total: {}%".format(p*100.0/len(close)))

plt.plot(dclose, color="red")
plt.plot(close, color="green")
plt.show()



'''
htr,hcyc = hp.hpfilter(dclose,100)
htr1, hcyc1 = hp.hpfilter(htr, 100)

h = np.array([hcyc, hcyc1, htr1]).swapaxes(0, 1)

fig, ax = plt.subplots(4,1)

ax[0].plot(hcyc[-100:], color="green")

ax[1].plot(hcyc1[-100:], color="green")

ax[2].plot(htr1[-100:], color="green")

#ax[3].plot(np.sum(h, axis=1), color="green")
ax[3].plot(dclose, color="black")

plt.show()
'''