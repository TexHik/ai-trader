import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.regularizers import Regularizer
from tensorflow.keras.utils import plot_model
import matplotlib.pyplot as plt
from CONV.Model import *
from Dataset import *
import os
def sgn(a):
    if (a>0):
        return 1
    elif(a<0):
        return -1
    else:
        return 0

BATCH_SIZE=90
WINDOW_SIZE=25

dataset = Dataset(windowsize=WINDOW_SIZE)
(data_x, data_y, data_min, data_max) = dataset.getTestData()
length = len(data_x)
predictor = BinaryPredictor(window_size=WINDOW_SIZE)
#predictor.load()

with tf.GradientTape() as grad_tape:
    input = tf.cast(data_x[0:1], dtype='float64')
    grad_tape.watch(input)
    result = predictor.predictor(input)
    #x = tf.reshape(x,[1,3])
    grads = grad_tape.gradient(result, input)
    print(np.sum(grads[0], axis=1))

result = []
for i in range(int(data_x.shape[0]/BATCH_SIZE)):
    t = np.minimum(BATCH_SIZE * (i + 1),length)
    x = data_x[BATCH_SIZE * i:t]
    result.append(predictor.predictor(x))
result = np.array(result).reshape(-1, 3)
target = (data_y[:result.shape[0]])

result = Dataset.denorm(result,data_min[:result.shape[0]],data_max[:result.shape[0]])
target = Dataset.denorm(target,data_min[:target.shape[0]],data_max[:target.shape[0]])

tresult = np.sum(result,axis=1)
ttarget = np.sum(target,axis=1)


p=0
for i in range(result.shape[0]-1):
    if (sgn(tresult[i+1]-tresult[i]) == sgn(ttarget[i+1]-ttarget[i])):
        p += 1


print("Trend Accuracy Total: {}%".format(p*100.0/(result.shape[0])))
print("MSE: {}".format(np.sqrt(np.mean(np.square(ttarget-tresult)))))
fig, ax = plt.subplots(2,1)

ax[0].plot(ttarget,color="black")
ax[0].plot(tresult, color='red')
plt.show()