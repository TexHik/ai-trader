import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.regularizers import Regularizer
from tensorflow.keras.utils import plot_model
import matplotlib.pyplot as plt
from Model import *
from Dataset import *
import os
def sgn(a):
    if (a>0):
        return 1
    elif(a<0):
        return -1
    else:
        return 0

BATCH_SIZE=90
WINDOW_SIZE=25
LENGTH=WINDOW_SIZE+10

dataset = Dataset(windowsize=WINDOW_SIZE)
data_x = dataset.getSequentialData(LENGTH)
predictor = PatternPredictor(window_size=WINDOW_SIZE)
predictor.load()

origin = np.array([data_x[i] for i in range(data_x.shape[0])]).sum(axis=1)

result = [data_x[i] for i in range(WINDOW_SIZE)]
for i in range(LENGTH-WINDOW_SIZE):
    print("{}/{}".format(i,LENGTH-WINDOW_SIZE))
    x = np.array(result[i:i+WINDOW_SIZE])

    min = np.min(x, axis=0, keepdims=True)
    max = np.max(x, axis=0, keepdims=True)
    x = (x - min) / (max - min + 1e-8) * 2 - 1

    r = predictor.predictor(np.expand_dims(x,axis=0))
    r = (r + 1) / 2 * (max - min) + min
    result.append(np.array(r[0]))
result = np.array(result).reshape(-1, 3)

tresult = np.sum(result,axis=1)


print("MSE: {}".format(np.sqrt(np.mean(np.square(tresult-origin)))))

fig, ax = plt.subplots(2,1)

ax[0].plot(origin,color="black")
ax[0].plot(tresult, color='red')
plt.show()
