import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.regularizers import Regularizer
from tensorflow.keras.utils import plot_model
import matplotlib.pyplot as plt
from RNN.Model import *
from Dataset import *
import os

BATCH_SIZE = 800
WINDOW_SIZE = 25
LEARNING_RATE = 1e-3

dataset = Dataset(windowsize=WINDOW_SIZE)

size = dataset.getLength()
print("Dataset loaded")
predictor = PatternPredictor(window_size=WINDOW_SIZE, lr=LEARNING_RATE, training=True)
predictor.load()

while (True):
    for i in range(int(size/BATCH_SIZE)):
        (data_x,data_y, data_min, data_max) = dataset.getTrainData(i*BATCH_SIZE, BATCH_SIZE)
        loss,result = predictor.train_step(data_x, data_y, LEARNING_RATE)
        print("[{}]-[{}] [{}/{}]Loss: {}".format(predictor.epoch,predictor.trainstep, i, int(size/BATCH_SIZE)-1, loss))
        if (predictor.trainstep%2==0):
            print("Saving")
            predictor.save()
            print("Model saved")
    print("Saving")
    predictor.save()
    print("Model saved")

