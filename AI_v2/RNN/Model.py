import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.regularizers import Regularizer
from tensorflow.keras.utils import plot_model
import numpy as np
import os
#TODO Decrease lstm dimentionaly
os.environ["PATH"] += os.pathsep + 'C:/Users/TexHik/AppData/Local/Programs/Python/Python36/graph/bin'

class RBF(tf.keras.layers.Layer):
    def __init__(self):
        super(RBF, self).__init__()
    def build(self,input_shape):
        super(RBF, self).build(input_shape)
    def call(self, x):
        return 1.0/(1+tf.square(x))
class PatternPredictor():
    def __init__(self,
                 window_size,
                 filters = 45,
                 depth = 5,
                 lr=1e-3,
                 training = False
                 ):
        self.window_size = window_size
        self.filters = filters
        self.depth = depth
        self.lr = lr

        self.predictor = self._predictor()
        self.optimizer = tf.keras.optimizers.Adam(self.lr)
        self.trainstep = 0
        self.epoch = 0
        if (training):
            self.writer = tf.summary.create_file_writer("./RNN/tensorboard/")

        print("Predictor created depth {} \n".format(self.depth))
        print("Total amount of predictor trainable parameters: {}\n"
            .format(
            np.sum([np.prod(v.get_shape().as_list()) for v in self.predictor.trainable_variables]),
        ))

        plot_model(self.predictor, to_file='RNN/predictor.png', show_shapes=True, show_layer_names=True)

    def _predictor(self):
        i1 = layers.Input(shape=[self.window_size,3])
        l1 = i1[:, :, 0]
        l2 = i1[:, :, 1]
        l3 = i1[:, :, 2]
        l1 = tf.expand_dims(l1,axis=2)
        l2 = tf.expand_dims(l2, axis=2)
        l3 = tf.expand_dims(l3, axis=2)

        for i in range(self.depth-1):
            l1 = layers.LSTM(self.filters, return_sequences=True, kernel_initializer='he_normal', dropout=0.1, recurrent_dropout=0.1)(l1)
            l2 = layers.LSTM(self.filters, return_sequences=True, kernel_initializer='he_normal', dropout=0.1, recurrent_dropout=0.1)(l2)
            l3 = layers.LSTM(self.filters, return_sequences=True, kernel_initializer='he_normal', dropout=0.1, recurrent_dropout=0.1)(l3)

        l1 = layers.LSTM(self.filters, kernel_initializer='he_normal', dropout=0.1, recurrent_dropout=0.1)(l1)
        o1 = layers.Dense(self.filters, kernel_initializer='he_normal', use_bias=True)(l1)
        o1 = layers.Dense(1, use_bias=True)(o1)

        l2 = layers.LSTM(self.filters, kernel_initializer='he_normal', dropout=0.1, recurrent_dropout=0.1)(l2)
        o2 = layers.Dense(self.filters, kernel_initializer='he_normal', use_bias=True)(l2)
        o2 = layers.Dense(1, use_bias=True)(o2)

        l3 = layers.LSTM(self.filters, kernel_initializer='he_normal', dropout=0.1, recurrent_dropout=0.1)(l3)
        o3 = layers.Dense(self.filters, kernel_initializer='he_normal', use_bias=True)(l3)
        o3 = layers.Dense(1, use_bias=True)(o3)

        o = layers.Concatenate(axis=1)([o1,o2,o3])
        return tf.keras.Model(inputs = i1, outputs = o)

    def loss(self, result, target):
        l1 = tf.reduce_mean(tf.square((target[:, 0] - result[:, 0])), axis=[0], keepdims=False)
        l2 = tf.reduce_mean(tf.square((target[:, 1] - result[:, 1])), axis=[0], keepdims=False)
        l3 = tf.reduce_mean(tf.square((target[:, 2] - result[:, 2])), axis=[0], keepdims=False)
        l = tf.sqrt(l1+l2+l3)
        return l,l1,l2,l3

    def train_step(self, input, target, lr=1e-3):
        if (lr!=self.lr):
            self.lr = lr
            self.optimizer = tf.keras.optimizers.Adam(self.lr)
        with tf.GradientTape() as grad_tape:
            with tf.device("/gpu:0"):
                result = tf.cast(self.predictor(input), dtype='float64')
                target = tf.cast(target, dtype='float64')

                l,l1,l2,l3 = self.loss(result, target)
                with (self.writer.as_default()):
                    tf.summary.scalar("Loss", l,
                                      step=self.trainstep)
                    tf.summary.scalar("loss l1", l1,
                                      step=self.trainstep)
                    tf.summary.scalar("loss l2", l2,
                                      step=self.trainstep)
                    tf.summary.scalar("loss l3", l3,
                                      step=self.trainstep)
                    tf.summary.histogram("result", result, step=self.trainstep)
                    self.writer.flush()
                grads = grad_tape.gradient(l, self.predictor.trainable_variables)
                self.optimizer.apply_gradients(zip(grads, self.predictor.trainable_variables))
                self.trainstep+=1
                return l, result
    def save(self):
        np.save("./model/info.npy",[self.trainstep,self.epoch])
        self.predictor.save_weights("./model/")
    def load(self):
        data = np.load("./model/info.npy")
        self.trainstep = data[0]
        self.epoch = data[1]
        self.predictor.load_weights("./model/")